<?php
namespace App\Repository;

use Illuminate\Database\Eloquent\Model;

interface ProductRepositoryInterface
{
   /**
    * @param array $attributes
    * @return Model
    */
    public function create(array $attributes): Model;

    /**
    * @param array $attributes
    * @param $id
    * @return Model
    */
    public function update(array $attributes, $id): Model;

    /**
     * @param $id
     */
    public function delete($id);
}