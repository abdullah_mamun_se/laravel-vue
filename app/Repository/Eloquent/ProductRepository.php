<?php

namespace App\Repository\Eloquent;

use App\Models\Product;
use App\Repository\ProductRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class ProductRepository extends BaseRepository implements ProductRepositoryInterface
{

   /**
    * UserRepository constructor.
    *
    * @param Product $model
    */
   public function __construct(Product $model)
   {
       parent::__construct($model);
   }

   /**
    * @param array $attributes
    *
    * @return Model
    */
    public function create(array $attributes): Model
    {
        return $this->model->create($attributes);
    }

    /**
    * @param array $attributes
    * @param $id
    * @return Model
    */
    public function update(array $attributes, $id): Model
    {
        $product = $this->model->find($id);
        $product->update($attributes);
        return $product;
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        $this->model->find($id)->delete();

        return response('Product deleted!');
    }

}