<?php

namespace App\Http\Controllers;

use App\Repository\ProductRepositoryInterface;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    private $productRepository;
    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }
    public function index()
    {
        $products = $this->productRepository->all()->toArray();
        return array_reverse($products);      
    }

    public function store(Request $request)
    {
        $product = $this->productRepository->create($request->all());

        return response()->json('Product created!');
    }

    public function show($id)
    {
        $product = $this->productRepository->find($id);
        return response()->json($product);
    }

    public function update($id, Request $request)
    {
        $this->productRepository->update($request->all(), $id);

        return response()->json('Product updated!');
    }

    public function destroy($id)
    {
        return $this->productRepository->delete($id);
    }
}
